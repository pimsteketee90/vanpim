export default {
  target: 'static',

  server: {
    host: '0.0.0.0'
  },

  // PAGE HEAD
  head: {
    link: [
      {
        rel: 'stylesheet',
        href:'https://fonts.googleapis.com/css2?family=Mada:wght@200;300;400;500;600;700;900&display=swap'
      },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ],

    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    ],
  },

  // GLOBAL CSS
  css: [
    './assets/scss/main.scss'
  ],

  buildModules: [
    '@nuxtjs/google-analytics'
  ],

  googleAnalytics: {
    id: 'UA-181896813-1'
  }
}